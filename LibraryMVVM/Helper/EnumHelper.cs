﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMVVM.Helper
{
    class EnumHelper
    {
        public static string GetDescription(Enum en)
        {
            var enumType = en.GetType();

            MemberInfo[] memberInfo = enumType.GetMember(en.ToString());

            var descriptionAttributeIndex = 0;

            if (memberInfo != null && memberInfo.Length > 0)
            {
                object[] attributes = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attributes != null && attributes.Length > 0)
                {
                    return ((DescriptionAttribute)attributes[descriptionAttributeIndex]).Description;
                }
            }

            return en.ToString();
        }
    }
}
