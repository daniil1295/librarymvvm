﻿using LibraryMVVM.Model;
using LibraryMVVM.Model.DBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMVVM.Repository
{
    public class JournalNoteRepository
    {
        LibraryDatabase _libraryDatabase = DatabaseDistributor.GetDatabase();
        public ICollection<JournalNote> GetAllJournalNotes()
        {
            return _libraryDatabase.JournalNote.ToList();
        }
        public void CreateNewNote(string visitorName, string visitorPhone, int bookId, int noteType)
        {
            var newNote = new JournalNote();
            newNote.JournalNoteVisitorName = visitorName;
            newNote.JournalNoteVisitorPhone = visitorPhone;
            newNote.BookId = bookId;
            newNote.NoteType = noteType;
            newNote.JournalNoteTakenDate = DateTime.Now;
            _libraryDatabase.JournalNote.Add(newNote);
            _libraryDatabase.SaveChanges();
        }
        public void RemoveJournalNoteByBookId(int bookId)
        {
            var notesToRemove = _libraryDatabase.JournalNote.Where(journalNote => journalNote.BookId == bookId);
            foreach (var note in notesToRemove)
            {
                _libraryDatabase.JournalNote.Remove(note);
            }
            _libraryDatabase.SaveChanges();
        }
        public void SetJournalNoteTypeReturned(int journalNoteId, int noteType)
        {
            var journalNotesSet = _libraryDatabase.JournalNote.Find(journalNoteId);
            journalNotesSet.JournalNoteReturnedDate = DateTime.Now;
            journalNotesSet.NoteType = noteType;
            _libraryDatabase.SaveChanges();
        }
    }
}
