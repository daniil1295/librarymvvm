﻿using LibraryMVVM.Model;
using LibraryMVVM.Model.DBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMVVM.Repository
{
    public class BookRepository
    {
        LibraryDatabase _libraryDatabase = DatabaseDistributor.GetDatabase();
        public ICollection<Book> GetAllBooks()
        {
            return _libraryDatabase.Book.ToList();
        }
        public int CreateNewBook(string bookISBN, string bookName, int bookPagesCount, double bookPrice, int bookPrinting, string bookSizeMillimeters, int bookYearOfWriting, int CoverType, int Language, int PublishingHouse, byte[] bookCoverPicture)
        {
            var newBook = new Book();
            newBook.BookCoverPicture = bookCoverPicture;
            newBook.BookISBN = bookISBN;
            newBook.BookName = bookName;
            newBook.BookPagesCount = bookPagesCount;
            newBook.BookPrice = bookPrice;
            newBook.BookPrinting = bookPrinting;
            newBook.BookSizeMillimeters = bookSizeMillimeters;
            newBook.BookYearOfWriting = bookYearOfWriting;
            newBook.CoverType = CoverType;
            newBook.Language = Language;
            newBook.PublishingHouse = PublishingHouse;
            _libraryDatabase.Book.Add(newBook);
            _libraryDatabase.SaveChanges();
            return newBook.BookId;
        }
        public void EditBook(string bookISBN, string bookName, int bookPagesCount, double bookPrice, int bookPrinting, string bookSizeMillimeters, int bookYearOfWriting, int CoverType, int Language, int PublishingHouse, byte[] bookCoverPicture,int bookId)
        {
            var Book = _libraryDatabase.Book.Find(bookId);
            Book.BookCoverPicture = bookCoverPicture;
            Book.BookISBN = bookISBN;
            Book.BookName = bookName;
            Book.BookPagesCount = bookPagesCount;
            Book.BookPrice = bookPrice;
            Book.BookPrinting = bookPrinting;
            Book.BookSizeMillimeters = bookSizeMillimeters;
            Book.BookYearOfWriting = bookYearOfWriting;
            Book.CoverType = CoverType;
            Book.Language = Language;
            Book.PublishingHouse = PublishingHouse;
            _libraryDatabase.SaveChanges();
        }
        public void RemoveBook(int bookId)
        {
            var Book = _libraryDatabase.Book.Find(bookId);
            _libraryDatabase.Book.Remove(Book);
            _libraryDatabase.SaveChanges();
        }
    }
}
