﻿using LibraryMVVM.Model;
using LibraryMVVM.Model.DBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMVVM.Repository
{
    public class GenreBookRepository
    {
        private LibraryDatabase _libraryDatabase = DatabaseDistributor.GetDatabase();
        public void CreateNewGenreBookNote(List<int> genresIds, int bookId)
        {
            for (var index = 0; index < genresIds.Count; index++)
            {
                var genreBook = new GenreBook() { Genre = genresIds[index], BookId = bookId };
                _libraryDatabase.GenreBook.Add(genreBook);
            }
            _libraryDatabase.SaveChanges();
        }

        public void RemoveGenreBookNote(List<int> genresIds, int bookId)
        {
            for (var index = 0; index < genresIds.Count; index++)
            {
                var genre = genresIds[index];
                var genreBookNote = _libraryDatabase.GenreBook.Where(genreBook => genreBook.Genre == genre && genreBook.BookId == bookId).FirstOrDefault();
                _libraryDatabase.GenreBook.Remove(genreBookNote);
            }
            _libraryDatabase.SaveChanges();
        }
        public void RemoveGenreBookNoteByBookId(int bookId)
        {
            var notesToRemove = _libraryDatabase.GenreBook.Where(genreBook => genreBook.BookId == bookId);
            foreach (var note in notesToRemove)
            {
                _libraryDatabase.GenreBook.Remove(note);
            }
            _libraryDatabase.SaveChanges();
        }
    }
}
