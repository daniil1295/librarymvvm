﻿using LibraryMVVM.Model;
using LibraryMVVM.Model.DBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMVVM.Repository
{
    public class AuthorRepository
    {
        LibraryDatabase _libraryDatabase = DatabaseDistributor.GetDatabase();
        public ICollection<Author> GetAllAuthors()
        {
            return _libraryDatabase.Author.ToList();
        }
    }
}
