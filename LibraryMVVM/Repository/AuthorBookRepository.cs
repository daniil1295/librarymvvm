﻿using LibraryMVVM.Model;
using LibraryMVVM.Model.DBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMVVM.Repository
{
    public class AuthorBookRepository
    {
        private LibraryDatabase _libraryDatabase = DatabaseDistributor.GetDatabase();
        public void CreateNewAuthorToBookNote(List<int> authorsIds, int bookId)
        {
            for (var index = 0; index < authorsIds.Count; index++)
            {
                var authorBook = new AuthorBook() { AuthorId = authorsIds[index], BookId = bookId };
                _libraryDatabase.AuthorBook.Add(authorBook);
            }
            _libraryDatabase.SaveChanges();
        }
        public void RemoveAuthorBookNote(List<int> authorsIds, int bookId)
        {
            for (var index = 0; index < authorsIds.Count; index++)
            {
                var authorId = authorsIds[index];
                var authorBookNote = _libraryDatabase.AuthorBook.Where(authorBook => authorBook.AuthorId == authorId && authorBook.BookId == bookId).FirstOrDefault();
                _libraryDatabase.AuthorBook.Remove(authorBookNote);
            }
            _libraryDatabase.SaveChanges();
        }
        public void RemoveAuthorBookNoteByBookId(int bookId)
        {
            var notesToRemove = _libraryDatabase.AuthorBook.Where(authorBook => authorBook.BookId == bookId);
            foreach (var note in notesToRemove)
            {
                _libraryDatabase.AuthorBook.Remove(note);
            }
            _libraryDatabase.SaveChanges();
        }
    }
}
