﻿using LibraryMVVM.Model.Enum;
using LibraryMVVM.Model.Interface;
using LibraryMVVM.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace LibraryMVVM.ViewModel
{
    public class ViewModelTakeBook : ViewModelBase, IRequestCloseViewModel
    {
        //events
        public event EventHandler RequestClose;

        //repositories
        private JournalNoteRepository _journalNoteRepository = new JournalNoteRepository();

        //helpers

        //commands
        public ICommand TakeBookCommand { get; set; }

        //props
        private int _bookId;

        private string _visitorName;
        public string VisitorName
        {
            get { return _visitorName; }
            set
            {
                _visitorName = value;
                OnPropertyChanged("VisitorName");
            }
        }

        private string _visitorPhone;
        public string VisitorPhone
        {
            get { return _visitorPhone; }
            set
            {
                _visitorPhone = value;
                OnPropertyChanged("VisitorPhone");
            }
        }

        //constructor
        public ViewModelTakeBook()
        {
            TakeBookCommand = new RelayCommand(arg => TakeBookMethod());
        }

        public ViewModelTakeBook(int bookId)
        {
            _bookId = bookId;
            TakeBookCommand = new RelayCommand(arg => TakeBookMethod());
        }
        //methods for ICommands
        private void TakeBookMethod()
        {
            if (!IsValidInfo())
            {
                MessageBox.Show("Please, fill information about you");
                return;
            }
            _journalNoteRepository.CreateNewNote(VisitorName, VisitorPhone, _bookId, (int)NoteTypes.Taken);
            MessageBox.Show("You successfully take a book, thanks!");
            RequestClose(this, new EventArgs());
        }

        //another methods
        private bool IsValidInfo()
        {
            if (string.IsNullOrWhiteSpace(VisitorName) || string.IsNullOrWhiteSpace(VisitorPhone))
            {
                return false;
            }
            return true;
        }
    }
}
