﻿using LibraryMVVM.Model;
using LibraryMVVM.Model.Enum;
using LibraryMVVM.Model.Interface;
using LibraryMVVM.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace LibraryMVVM.ViewModel
{
    public class ViewModelShowJournalNotes : ViewModelBase
    {
        //repositories
        private JournalNoteRepository _journalNoteRepository = new JournalNoteRepository();

        //commands
        public ICommand ChangeStateCommand { get; set; }

        //props
        private ICollection<JournalNote> _journalNotes;
        public ICollection<JournalNote> JournalNotes
        {
            get { return _journalNotes; }
            set
            {
                _journalNotes = value;
                OnPropertyChanged("JournalNotes");
            }
        }

        private JournalNote _selectedJournalNote;
        public JournalNote SelectedJournalNote
        {
            get { return _selectedJournalNote; }
            set
            {
                _selectedJournalNote = value;
                OnPropertyChanged("SelectedJournalNote");
            }
        }

        //constructor
        public ViewModelShowJournalNotes()
        {
            ChangeStateCommand = new RelayCommand(arg => ChangeStateMethod());
            UpdateJournalNotes();
        }

        //methods for ICommands

        private void ChangeStateMethod()
        {
            if(SelectedJournalNote.NoteType==(int)NoteTypes.Returned)
            {
                MessageBox.Show("This book already returned");
                return;
            }
            _journalNoteRepository.SetJournalNoteTypeReturned(SelectedJournalNote.JournalNoteId, (int)NoteTypes.Returned);
            UpdateJournalNotes();
        }
        
        //another methods
        private void UpdateJournalNotes()
        {
            JournalNotes = _journalNoteRepository.GetAllJournalNotes();
            SelectedJournalNote = JournalNotes.First();
        }
    }
}
