﻿using LibraryMVVM.Model;
using LibraryMVVM.Model.Interface;
using LibraryMVVM.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace LibraryMVVM.ViewModel
{
    public class ViewModelSearchAuthor : ViewModelBase, IRequestCloseViewModel
    {
        //repositories
        private AuthorRepository _authorRepository = new AuthorRepository();

        //events
        public event EventHandler RequestClose;

        //commands
        public ICommand ConfirmSelectedAuthorCommand { get; set; }
        public ICommand CancelCommand { get; set; }
        public ICommand SearchAuthorsCommand { get; set; }
        public ICommand ShowAllAuthorsCommand { get; set; }

        //props
        private ObservableCollection<Author> _authors;
        public ObservableCollection<Author> Authors
        {
            get { return _authors; }
            set
            {
                _authors = value;
                OnPropertyChanged("Authors");
            }
        }

        private Author _confirmedSelectedAuthor = null;
        public Author ConfirmedSelectedAuthor
        {
            get { return _confirmedSelectedAuthor; }
            set { _confirmedSelectedAuthor = value; }
        }

        private string _conditionString;
        public string ConditionString
        {
            get { return _conditionString; }
            set
            {
                _conditionString = value;

                OnPropertyChanged("ConditionString");
            }
        }

        private Author _selectedAuthor;
        public Author SelectedAuthor
        {
            get { return _selectedAuthor; }
            set
            {
                _selectedAuthor = value;
                OnPropertyChanged("SelectedAuthor");
            }
        }

        //constructor
        public ViewModelSearchAuthor()
        {
            ConfirmSelectedAuthorCommand = new RelayCommand(arg=>ConfirmSelectedAuthorMethod());
            CancelCommand = new RelayCommand(arg => CancelMethod());
            SearchAuthorsCommand = new RelayCommand(arg => SearchAuthorsMethod());
            ShowAllAuthorsCommand = new RelayCommand(arg => ShowAllAuthorsMethod());
            ShowAllAuthorsMethod();
        }

        //methods for ICommands
        private void ShowAllAuthorsMethod()
        {
            Authors = new ObservableCollection<Author>(GetAuthors());
        }
        private void ConfirmSelectedAuthorMethod()
        {
            if(SelectedAuthor==null)
            {
                MessageBox.Show("Author is not selected");
                return;
            }
            ConfirmedSelectedAuthor = SelectedAuthor;
            RequestClose(this, new EventArgs());
        }

        private void CancelMethod()
        {
            RequestClose(this, new EventArgs());
        }

        private void SearchAuthorsMethod()
        {
            if (string.IsNullOrWhiteSpace(ConditionString))
            {
                return;
            }
            Authors = new ObservableCollection<Author>(GetAuthors().Where(author => author.ToString().Contains(ConditionString)));
            OnPropertyChanged("Authors");
        }

        //another methods
        private ICollection<Author> GetAuthors()
        {
            return _authorRepository.GetAllAuthors();
        }
        
    }
}
