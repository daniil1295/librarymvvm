﻿using LibraryMVVM.Model;
using LibraryMVVM.Repository;
using LibraryMVVM.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace LibraryMVVM.ViewModel
{
    public class ViewModelMainWindow : ViewModelBase
    {
        //repositories
        private JournalNoteRepository _journalNoteRepository = new JournalNoteRepository();
        private AuthorBookRepository _authorBookRepository = new AuthorBookRepository();
        private GenreBookRepository _genreBookRepository = new GenreBookRepository(); 
        private BookRepository _bookRepository = new BookRepository();

        //commands
        public ICommand ShowJournalCommand { get; set; }
        public ICommand CreateBookCommand { get; set; }
        public ICommand DeleteBookCommand { get; set; }
        public ICommand EditBookCommand { get; set; }
        public ICommand TakeBookCommand { get; set; }

        //props
        private ICollection<Book> _books;
        public ICollection<Book> Books
        {
            get { return _books; }
            set
            {
                _books = value;
                OnPropertyChanged("Books");
            }
        }

        private Book _selectedBook;
        public Book SelectedBook
        {
            get { return _selectedBook; }
            set
            {
                _selectedBook = value;
                OnPropertyChanged("SelectedBook");
            }
        }
        //constructor
        public ViewModelMainWindow()
        {
            CreateBookCommand = new RelayCommand(arg => NewBookMethod());
            EditBookCommand = new RelayCommand(arg => EditBookMethod());
            DeleteBookCommand = new RelayCommand(arg => DeleteBookMethod());
            TakeBookCommand = new RelayCommand(arg => TakeBookMethod());
            ShowJournalCommand = new RelayCommand(arg => ShowJournalMethod());
            UpdateBooks();
        }

        //methods for ICommands
        private void NewBookMethod()
        {
            var viewModelCreateBook = new ViewModelCreateBook();
            ViewRequest.RequestAddForm(viewModelCreateBook);
            UpdateBooks();
        }

        private void EditBookMethod()
        {
            var viewModelEditBook = new ViewModelEditBook(SelectedBook);
            ViewRequest.RequestEditForm(viewModelEditBook);
            UpdateBooks();
        }

        private void DeleteBookMethod()
        {
            _authorBookRepository.RemoveAuthorBookNoteByBookId(SelectedBook.BookId);
            _genreBookRepository.RemoveGenreBookNoteByBookId(SelectedBook.BookId);
            _journalNoteRepository.RemoveJournalNoteByBookId(SelectedBook.BookId);
            _bookRepository.RemoveBook(SelectedBook.BookId);
            UpdateBooks();
        }

        private void TakeBookMethod()
        {
            var viewModelTakeBook = new ViewModelTakeBook(SelectedBook.BookId);
            ViewRequest.RequestTakeForm(viewModelTakeBook);
        }

        private void ShowJournalMethod()
        {
            var viewModelShowJournalNotes = new ViewModelShowJournalNotes();
            ViewRequest.RequestShowJournalNotesForm(viewModelShowJournalNotes);
        }

        //another methods
        private void UpdateBooks()
        {
            Books = _bookRepository.GetAllBooks();
            SelectedBook = Books.First();
        }
    }
}
