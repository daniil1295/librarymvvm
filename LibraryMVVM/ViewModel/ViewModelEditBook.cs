﻿using LibraryMVVM.Helper;
using LibraryMVVM.Model;
using LibraryMVVM.Model.Enum;
using LibraryMVVM.Model.Interface;
using LibraryMVVM.Repository;
using LibraryMVVM.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace LibraryMVVM.ViewModel
{
    public class ViewModelEditBook : ViewModelBase, IRequestCloseViewModel
    {
        //events
        public event EventHandler RequestClose;

        //helpers
        private ImageHelper _imageHelper = new ImageHelper();

        //repositories
        private BookRepository _bookRepository = new BookRepository();
        private AuthorBookRepository _authorBookRepository = new AuthorBookRepository();
        private GenreBookRepository _genreBookRepository = new GenreBookRepository();

        //commands
        public ICommand AddGenreCommand { get; set; }
        public ICommand DeleteGenreCommand { get; set; }
        public ICommand AddAuthorCommand { get; set; }
        public ICommand DeleteAuthorCommand { get; set; }
        public ICommand OpenPhotoCommand { get; set; }
        public ICommand EditBookCommand { get; set; }

        //props
        private List<int> _authorsIdsOld = new List<int>();
        private List<int> _genresIdsOld = new List<int>();
        private List<int> _authorsIdsCurrent = new List<int>();
        private List<int> _genresIdsCurrent = new List<int>();
        private int _bookId;

        private string _bookName;
        public string BookName
        {
            get { return _bookName; }
            set
            {
                _bookName = value;
                OnPropertyChanged("BookName");
            }
        }
        private string _bookPrice;
        public string BookPrice
        {
            get { return _bookPrice; }
            set
            {
                _bookPrice = value;
                OnPropertyChanged("BookPrice");
            }
        }
        private string _bookYearOfWriting;
        public string BookYearOfWriting
        {
            get { return _bookYearOfWriting; }
            set
            {
                _bookYearOfWriting = value;
                OnPropertyChanged("BookYearOfWriting");
            }
        }
        private string _bookPagesCount;
        public string BookPagesCount
        {
            get { return _bookPagesCount; }
            set
            {
                _bookPagesCount = value;
                OnPropertyChanged("BookPagesCount");
            }
        }

        private string _bookISBN;
        public string BookISBN
        {
            get { return _bookISBN; }
            set
            {
                _bookISBN = value;
                OnPropertyChanged("BookISBN");
            }
        }
        private string _bookSizeMillimeters;
        public string BookSizeMillimeters
        {
            get { return _bookSizeMillimeters; }
            set
            {
                _bookSizeMillimeters = value;
                OnPropertyChanged("BookSizeMillimeters");
            }
        }
        private string _bookPrinting;
        public string BookPrinting
        {
            get { return _bookPrinting; }
            set
            {
                _bookPrinting = value;
                OnPropertyChanged("BookPrinting");
            }
        }

        private BitmapImage _bookCoverImage;
        public BitmapImage BookCoverImage
        {
            get { return _bookCoverImage; }
            set
            {
                _bookCoverImage = value;
                OnPropertyChanged("BookCoverImage");
            }
        }

        private ObservableCollection<Genres> _selectedGenres = new ObservableCollection<Genres>();
        public ObservableCollection<Genres> SelectedGenres
        {
            get { return _selectedGenres; }
            set
            {
                _selectedGenres = value;
                OnPropertyChanged("SelectedGenres");
            }
        }
        private ObservableCollection<ShortAuthorInfo> _selectedAuthors = new ObservableCollection<ShortAuthorInfo>();
        public ObservableCollection<ShortAuthorInfo> SelectedAuthors
        {
            get { return _selectedAuthors; }
            set
            {
                _selectedAuthors = value;
                OnPropertyChanged("SelectedAuthors");
            }
        }
        private ShortAuthorInfo _selectedAuthorFromList;
        public ShortAuthorInfo SelectedAuthorFromList
        {
            get { return _selectedAuthorFromList; }
            set
            {
                _selectedAuthorFromList = value;
                OnPropertyChanged("SelectedAuthorFromList");
            }
        }
        private Genres _selectedGenreFromList;
        public Genres SelectedGenreFromList
        {
            get { return _selectedGenreFromList; }
            set
            {
                _selectedGenreFromList = value;
                OnPropertyChanged("SelectedGenreFromList");
            }
        }
        private Languages _selectedLanguage;
        public Languages SelectedLanguage
        {
            get { return _selectedLanguage; }
            set
            {
                _selectedLanguage = value;
                OnPropertyChanged("SelectedLanguage");
            }
        }
        private CoverTypes _selectedCoverType;
        public CoverTypes SelectedCoverType
        {
            get { return _selectedCoverType; }
            set
            {
                _selectedCoverType = value;
                OnPropertyChanged("SelectedCoverType");
            }
        }
        private PublishingHouses _selectedPublishingHouse;
        public PublishingHouses SelectedPublishingHouse
        {
            get { return _selectedPublishingHouse; }
            set
            {
                _selectedPublishingHouse = value;
                OnPropertyChanged("SelectedPublishingHouse");
            }
        }
        private Genres _selectedGenre;
        public Genres SelectedGenre
        {
            get { return _selectedGenre; }
            set
            {
                _selectedGenre = value;
                OnPropertyChanged("SelectedGenre");
            }
        }
        //constructor
        public ViewModelEditBook()
        {
            AddGenreCommand = new RelayCommand(arg => AddGenreMethod());
            DeleteGenreCommand = new RelayCommand(arg => DeleteGenreMethod());
            AddAuthorCommand = new RelayCommand(arg => AddAuthorMethod());
            OpenPhotoCommand = new RelayCommand(arg => OpenPhotoMethod());
            DeleteAuthorCommand = new RelayCommand(arg => DeleteAuthorMethod());
            EditBookCommand = new RelayCommand(arg => EditBookMethod());
        }
        public ViewModelEditBook(Book selectedBook)
        {
            _bookId = selectedBook.BookId;
            BookCoverImage = _imageHelper.ByteArrayToImageSource(selectedBook.BookCoverPicture);
            BookISBN = selectedBook.BookISBN;
            BookName = selectedBook.BookName;
            BookPagesCount = selectedBook.BookPagesCount.ToString();
            BookPrice = selectedBook.BookPrice.ToString();
            BookPrinting = selectedBook.BookPrinting.ToString();
            BookSizeMillimeters = selectedBook.BookSizeMillimeters;
            BookYearOfWriting = selectedBook.BookYearOfWriting.ToString();
            SelectedCoverType = (CoverTypes)selectedBook.CoverType;
            SelectedLanguage = (Languages)selectedBook.Language;
            SelectedPublishingHouse = (PublishingHouses)selectedBook.PublishingHouse;

            SelectedGenre = Genres.Classic;

            LoadGenres(selectedBook.GenreBook);
            LoadAuthors(selectedBook.AuthorBook);

            AddGenreCommand = new RelayCommand(arg => AddGenreMethod());
            DeleteGenreCommand = new RelayCommand(arg => DeleteGenreMethod());
            AddAuthorCommand = new RelayCommand(arg => AddAuthorMethod());
            OpenPhotoCommand = new RelayCommand(arg => OpenPhotoMethod());
            DeleteAuthorCommand = new RelayCommand(arg => DeleteAuthorMethod());
            EditBookCommand = new RelayCommand(arg => EditBookMethod());
        }

        //methods for ICommands
        private void AddGenreMethod()
        {
            if (!SelectedGenres.Contains(SelectedGenre))
            {
                SelectedGenres.Add(SelectedGenre);
            }
            SelectedGenreFromList = SelectedGenre;
        }

        private void DeleteGenreMethod()
        {
            SelectedGenres.Remove(SelectedGenreFromList);
        }

        private void DeleteAuthorMethod()
        {
            SelectedAuthors.Remove(SelectedAuthorFromList);
        }

        private void AddAuthorMethod()
        {
            var viewModelSearchAuthor = new ViewModelSearchAuthor();
            ViewRequest.RequestSearchAuthorForm(viewModelSearchAuthor);
            if (viewModelSearchAuthor.ConfirmedSelectedAuthor == null)
            {
                return;
            }
            var selectedAuthor = viewModelSearchAuthor.ConfirmedSelectedAuthor.ToShortAuthorInfo();
            if (!SelectedAuthors.Cast<ShortAuthorInfo>().Any(author => author.AuthorId == selectedAuthor.AuthorId))
            {
                SelectedAuthors.Add(selectedAuthor);
            }
            SelectedAuthorFromList = selectedAuthor;
        }

        private void OpenPhotoMethod()
        {
            BookCoverImage = _imageHelper.OpenImage();
        }

        private void EditBookMethod()
        {
            if (!IsValidBookInfo())
            {
                MessageBox.Show("Wrong info");
                return;
            }
            FillCurrentAuthorsId();
            FillCurrentGenresId();
            StartEditTransaction();
        }

        //another methods
        private void StartEditTransaction()
        {
            _bookRepository.EditBook(BookISBN, BookName, Convert.ToInt32(BookPagesCount), Convert.ToDouble(BookPrice), Convert.ToInt32(BookPrinting), BookSizeMillimeters, Convert.ToInt32(BookYearOfWriting), (int)SelectedCoverType, (int)SelectedLanguage, (int)SelectedPublishingHouse, _imageHelper.BitmapToByteArray(BookCoverImage), _bookId);
            var genresIdsToRemove = GetGenresToRemove();
            var genresIdsToCreate = GetGenresToCreate();
            var authorsIdsToRemove = GetAuthorsToRemove();
            var authorsIdsToCreate = GetAuthorsToCreate();
            if (genresIdsToRemove.Count != 0)
            {
                _genreBookRepository.RemoveGenreBookNote(genresIdsToRemove, _bookId);
            }
            if (genresIdsToCreate.Count != 0)
            {
                _genreBookRepository.CreateNewGenreBookNote(genresIdsToCreate, _bookId);
            }
            if (authorsIdsToRemove.Count != 0)
            {
                _authorBookRepository.RemoveAuthorBookNote(authorsIdsToRemove, _bookId);
            }
            if (authorsIdsToCreate.Count != 0)
            {
                _authorBookRepository.CreateNewAuthorToBookNote(authorsIdsToCreate, _bookId);
            }
            MessageBox.Show("Success");
            RequestClose(this, new EventArgs());
        }

        private bool IsValidBookInfo()
        {
            if (BookCoverImage.UriSource == null&& BookCoverImage.StreamSource==null)
            {
                return false;
            }
            if (SelectedAuthors.Count() == 0 || SelectedGenres.Count() == 0)
            {
                return false;
            }
            if (string.IsNullOrWhiteSpace(BookISBN) || string.IsNullOrWhiteSpace(BookName) || string.IsNullOrWhiteSpace(BookPagesCount) || string.IsNullOrWhiteSpace(BookPrice) || string.IsNullOrWhiteSpace(BookPrinting) || string.IsNullOrWhiteSpace(BookSizeMillimeters) || string.IsNullOrWhiteSpace(BookYearOfWriting))
            {
                return false;
            }
            var tmpDouble = 0.0;
            var tmpInt = 0;
            if (!Double.TryParse(BookPrice, out tmpDouble) || !Int32.TryParse(BookPagesCount, out tmpInt) || !Int32.TryParse(BookYearOfWriting, out tmpInt) || !Int32.TryParse(BookPrinting, out tmpInt))
            {
                return false;
            }
            if (Convert.ToInt32(BookPagesCount) < 0 || Convert.ToInt32(BookYearOfWriting) < 0 || Convert.ToInt32(BookPrinting) < 0 || Convert.ToDouble(BookPrice) < 0)
            {
                return false;
            }
            return true;
        }

        private void LoadAuthors(ICollection<AuthorBook> authorBookCollection)
        {
            foreach(var authorBook in authorBookCollection)
            {
                SelectedAuthors.Add(authorBook.Author.ToShortAuthorInfo());
                _authorsIdsOld.Add(authorBook.Author.AuthorId);
            }
        }
        private void LoadGenres(ICollection<GenreBook> genreBookCollection)
        {
            foreach(var genreBook in genreBookCollection)
            {
                SelectedGenres.Add((Genres)genreBook.Genre);
                _genresIdsOld.Add(genreBook.Genre);
            }
        }
        private void FillCurrentGenresId()
        {
            foreach(var genre in SelectedGenres)
            {
                _genresIdsCurrent.Add((int)genre);
            }
        }
        private void FillCurrentAuthorsId()
        {
            foreach(var author in SelectedAuthors)
            {
                _authorsIdsCurrent.Add(author.AuthorId);
            }
        }
        private List<int> GetGenresToRemove()
        {
            var genresIdsToRemove = new List<int>();
            //find genreToBook connection which need to remove
            for (var index = 0; index < _genresIdsOld.Count; index++)
            {
                if (!_genresIdsCurrent.Contains(_genresIdsOld[index]))
                {
                    genresIdsToRemove.Add(_genresIdsOld[index]);
                }
            }
            return genresIdsToRemove;
        }

        private List<int> GetGenresToCreate()
        {
            var genresIdsToCreate = new List<int>();
            //find genreToBook connection which need to create
            for (var index = 0; index < _genresIdsCurrent.Count; index++)
            {
                if (!_genresIdsOld.Contains(_genresIdsCurrent[index]))
                {
                    genresIdsToCreate.Add(_genresIdsCurrent[index]);
                }
            }
            return genresIdsToCreate;
        }

        private List<int> GetAuthorsToRemove()
        {
            var authorsIdsToRemove = new List<int>();
            //find authorToBook connection which need to remove
            for (var index = 0; index < _authorsIdsOld.Count; index++)
            {
                if (!_authorsIdsCurrent.Contains(_authorsIdsOld[index]))
                {
                    authorsIdsToRemove.Add(_authorsIdsOld[index]);
                }
            }
            return authorsIdsToRemove;
        }

        private List<int> GetAuthorsToCreate()
        {
            var authorsIdsToCreate = new List<int>();
            //find authorToBook connection which need to create
            for (var index = 0; index < _authorsIdsCurrent.Count; index++)
            {
                if (!_authorsIdsOld.Contains(_authorsIdsCurrent[index]))
                {
                    authorsIdsToCreate.Add(_authorsIdsCurrent[index]);
                }
            }
            return authorsIdsToCreate;
        }
    }
}
