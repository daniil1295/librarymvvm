﻿using LibraryMVVM.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMVVM.View
{
    public static class ViewRequest
    {
        public static void RequestAddForm(ViewModelCreateBook viewModel)
        {
            var createBookWindow = new CreateBookWindow();
            viewModel.RequestClose += (s, e) => createBookWindow.Close();
            createBookWindow.DataContext = viewModel;
            createBookWindow.ShowDialog();
        }
        public static void RequestSearchAuthorForm(ViewModelSearchAuthor viewModel)
        {
            var searchAuthorWindow = new SearchAuthor();
            viewModel.RequestClose += (s, e) => searchAuthorWindow.Close();
            searchAuthorWindow.DataContext = viewModel;
            searchAuthorWindow.ShowDialog();
        }
        public static void RequestEditForm(ViewModelEditBook viewModel)
        {
            var editBookWindow = new EditBook();
            viewModel.RequestClose += (s, e) => editBookWindow.Close();
            editBookWindow.DataContext = viewModel;
            editBookWindow.ShowDialog();
        }
        public static void RequestTakeForm(ViewModelTakeBook viewModel)
        {
            var takeBookWindow = new TakeBook();
            viewModel.RequestClose += (s, e) => takeBookWindow.Close();
            takeBookWindow.DataContext = viewModel;
            takeBookWindow.ShowDialog();
        }
        public static void RequestShowJournalNotesForm(ViewModelShowJournalNotes viewModel)
        {
            var showJournalNotesWindow = new ShowJournalNotes();
            showJournalNotesWindow.DataContext = viewModel;
            showJournalNotesWindow.ShowDialog();
        }
    }
}
