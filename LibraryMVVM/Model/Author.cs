namespace LibraryMVVM.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Author")]
    public partial class Author
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Author()
        {
            AuthorBook = new HashSet<AuthorBook>();
        }

        public int AuthorId { get; set; }

        [Required]
        public string AuthorFirstName { get; set; }

        [Required]
        public string AuthorLastName { get; set; }

        public string AuthorPatronymic { get; set; }

        public string AuthorPenName { get; set; }

        public override string ToString()
        {
            return AuthorLastName + " " + AuthorFirstName + " " + AuthorPatronymic + " " + AuthorPenName;
        }
        public ShortAuthorInfo ToShortAuthorInfo()
        {
            return new ShortAuthorInfo() { AuthorId = this.AuthorId, AuthorFullName = this.ToString() };
        }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AuthorBook> AuthorBook { get; set; }
    }
}
