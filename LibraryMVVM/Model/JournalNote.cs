namespace LibraryMVVM.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JournalNote")]
    public partial class JournalNote
    {
        public int JournalNoteId { get; set; }

        public int BookId { get; set; }

        public DateTime JournalNoteTakenDate { get; set; }

        public int NoteType { get; set; }

        public string JournalNoteVisitorName { get; set; }

        public string JournalNoteVisitorPhone { get; set; }

        public DateTime? JournalNoteReturnedDate { get; set; }

        public virtual Book Book { get; set; }
    }
}
