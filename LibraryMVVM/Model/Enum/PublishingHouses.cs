﻿using LibraryMVVM.Formatter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMVVM.Model.Enum
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum PublishingHouses
    {
        [Description("Pearson")]
        Pearson = 1,
        [Description("Reed Elsevier")]
        Reed_Elsevier = 2,
        [Description("Thomson-Reuters")]
        ThomsonReuters = 3,
        [Description("Wolters Kluwer")]
        Wolters_Kluwer = 4,
        [Description("Random House")]
        Random_House = 5,
        [Description("Hachette Livre")]
        Hachette_Livre = 6,
        [Description("Grupo Planeta")]
        Grupo_Planeta = 7,
        [Description("McGraw-Hill Education")]
        McGrawHill_Education = 8,
        [Description("Holtzbrinck")]
        Holtzbrinck = 9,
        [Description("Scholastic")]
        Scholastic = 10,
        [Description("Cengage")]
        Cengage = 11,
        [Description("Wiley")]
        Wiley = 12,
        [Description("De Agostini Editore")]
        De_Agostini_Editore = 13
    }
}
