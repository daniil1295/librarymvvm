﻿using LibraryMVVM.Formatter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMVVM.Model.Enum
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum Genres
    {
        [Description("Classic")]
        Classic = 1,
        [Description("Fiction")]
        Fiction = 2,
        [Description("Non-Fiction")]
        Non_Fiction = 3,
        [Description("Scientific fiction")]
        Scientific_Finction = 4,
        [Description("Realism")]
        Realism = 5,
        [Description("Comedy")]
        Comedy = 6,
        [Description("Short-story")]
        Short_story = 7,
        [Description("Horror")]
        Horror = 8,
        [Description("Drama")]
        Drama = 9,
        [Description("Adventure")]
        Adventure = 10
    }
}
