﻿using LibraryMVVM.Formatter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMVVM.Model.Enum
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum Languages
    {
        [Description("Ukrainian")]
        Ukrainian = 1,
        [Description("Russian")]
        Russian = 2,
        [Description("Polska")]
        Polska = 3,
        [Description("English")]
        English = 4,
        [Description("Belarus")]
        Belarus = 5,
        [Description("German")]
        German = 6
    }
}
