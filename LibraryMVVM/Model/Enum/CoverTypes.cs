﻿using LibraryMVVM.Formatter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMVVM.Model.Enum
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum CoverTypes
    {
        [Description("Strong")]
        Strong = 1,
        [Description("Medium")]
        Medium = 2,
        [Description("Low")]
        Low = 3
    }
}
