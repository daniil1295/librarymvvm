﻿using LibraryMVVM.Formatter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMVVM.Model.Enum
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum NoteTypes
    {
        [Description("Taken")]
        Taken = 1,
        [Description("Returned")]
        Returned = 2
    }
}
