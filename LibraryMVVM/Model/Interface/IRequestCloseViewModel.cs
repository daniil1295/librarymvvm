﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMVVM.Model.Interface
{
    interface IRequestCloseViewModel
    {
        /// <summary>
        /// событие, отвечающее за закрытие окна
        /// </summary>
        event EventHandler RequestClose;
    }
}
