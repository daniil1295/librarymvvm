namespace LibraryMVVM.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GenreBook")]
    public partial class GenreBook
    {
        public int GenreBookId { get; set; }

        public int Genre { get; set; }

        public int BookId { get; set; }

        public virtual Book Book { get; set; }
    }
}
