﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMVVM.Model.DBContext
{
    public class DatabaseDistributor
    {
        private static LibraryDatabase _libraryDatabase = new LibraryDatabase();
        public static LibraryDatabase GetDatabase()
        {
            return _libraryDatabase;
        }
    }
}
