namespace LibraryMVVM.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Book")]
    public partial class Book
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Book()
        {
            AuthorBook = new HashSet<AuthorBook>();
            GenreBook = new HashSet<GenreBook>();
            JournalNote = new HashSet<JournalNote>();
        }

        public int BookId { get; set; }

        [Required]
        public string BookName { get; set; }

        public double BookPrice { get; set; }

        public int BookYearOfWriting { get; set; }

        public int BookPagesCount { get; set; }

        public int Language { get; set; }

        public int BookPrinting { get; set; }

        public int PublishingHouse { get; set; }

        [Required]
        public string BookISBN { get; set; }

        [Required]
        public string BookSizeMillimeters { get; set; }

        public int CoverType { get; set; }

        [Required]
        public byte[] BookCoverPicture { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AuthorBook> AuthorBook { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GenreBook> GenreBook { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JournalNote> JournalNote { get; set; }
    }
}
