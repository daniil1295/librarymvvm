﻿using LibraryMVVM.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace LibraryMVVM.Formatter
{
    public class BookAuthorsFormatter : IValueConverter
    {
        public object Convert(object authorBookList, Type targetType, object parameter, CultureInfo culture)
        {
            var allAuthors = "";


            foreach (var authorBook in (ICollection<AuthorBook>)authorBookList)
            {
                allAuthors += authorBook.Author.ToString() + ",\n";
            }
            var firstCharIndex = 0;
            var countOfCharsWhichNeedToCut = 2;
            if (allAuthors != "")
            {
                allAuthors = allAuthors.Substring(firstCharIndex, allAuthors.Length - countOfCharsWhichNeedToCut);
            }
            return allAuthors.Replace("  ,",",").Replace(" ,",",");
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
