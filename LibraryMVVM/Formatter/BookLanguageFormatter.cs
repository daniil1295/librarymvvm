﻿using LibraryMVVM.Helper;
using LibraryMVVM.Model.Enum;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace LibraryMVVM.Formatter
{ 
    public class BookLanguageFormatter : IValueConverter
    {
        public object Convert(object language, Type targetType, object parameter, CultureInfo culture)
        {
            return EnumHelper.GetDescription((Languages)language);
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
