﻿using LibraryMVVM.Helper;
using LibraryMVVM.Model;
using LibraryMVVM.Model.Enum;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace LibraryMVVM.Formatter
{
    public class BookGenresFormatter : IValueConverter
    {
        public object Convert(object genreBookSet, Type targetType, object parameter, CultureInfo culture)
        {
            var allGenres = "";
            foreach (var genreBook in (ICollection<GenreBook>)genreBookSet)
            {
                allGenres += EnumHelper.GetDescription((Genres)genreBook.Genre)+", ";
            }
            var firstCharIndex = 0;
            var countOfCharsWhichNeedToCut = 2;
            if (allGenres != "")
            {
                allGenres = allGenres.Substring(firstCharIndex, allGenres.Length - countOfCharsWhichNeedToCut);
            }
            return allGenres;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
